import { Component } from '@angular/core';
import { GridModule } from '@progress/kendo-angular-grid';
import { ClientsService } from '../clients.service';

@Component({
  selector: 'app-clients',
  standalone: true,
  imports: [GridModule],
  providers: [ClientsService],
  templateUrl: './clients.component.html',
  styleUrl: './clients.component.css',
})
export class ClientsComponent {
  constructor(private clientService: ClientsService) {
    clientService.getClients().subscribe((data) => {
      this.gridData=data;
    });
  }
  public gridData: any[] = [];
}
