import { Component } from '@angular/core';
import { RouterOutlet } from '@angular/router';
import { ClientsService } from './clients.service';
import { HttpClientModule } from '@angular/common/http';

@Component({
  selector: 'app-root',
  standalone: true,
  imports: [RouterOutlet, HttpClientModule],
  providers: [ClientsService],
  templateUrl: './app.component.html',
  styleUrl: './app.component.css'
})
export class AppComponent {
  constructor(private clientService: ClientsService) {
    clientService.getClients().subscribe((data) => {});
  }
  title = 'AsapTaskFrontend';
}
