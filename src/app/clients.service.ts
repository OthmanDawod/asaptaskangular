import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ClientsService {

  constructor(private http:HttpClient) { }

  public getClients() {
    return this.http.get<any>('http://localhost:42834/api/Client');
  }
}
